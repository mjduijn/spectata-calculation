package nl.tudelft.spectata.calculation.metrictoscore

import java.io.File

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{Build, Data, Metric}
import org.apache.commons.io.FileUtils
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.sql.{DataFrame, Dataset, SQLContext}

object ScoreMainOld {

  def toCsv(data: Dataset[Metric]): Unit = {


    val f = "target/metrics"
    FileUtils.deleteDirectory(new File(f))
    data
      .toDF()
      .coalesce(1)
      .write.format("com.databricks.spark.csv").option("header", "true").save(f)
  }

  def execute(cc: SQLContext) = {
    import cc.implicits._

//    val productionClasses = Data.getBuildData(cc).filter(_.rule == "LINES").flatMap(_.toSingleData())
//        .filter(!_.ref.path.startsWith("src/test"))
//        .map(pc => (pc.ref.path.split("/").last, pc.buildid))
//
    val m = Metric.getMetric(cc)
    //Only select unit or virtual tests
//    val m = Metric.getMetric(cc)
//      .filter(m => m.path.startsWith("src/test") || m.path.startsWith("VirtualTest"))
//      .map(m => ( m.path.split("/").last.replaceAll("(?i)test", ""), m)).as("ms")
//      .joinWith(productionClasses.as("pcs"), $"ms._1" === $"pcs._1" and $"ms._2.buildid" === $"pcs._2")
//      .map(_._1._2)
//
//      .union(Metric.getMetric(cc).filter(_.metric == "DependantChange")) //Select all changes
//      .union(Metric.getMetric(cc).filter(_.metric.toLowerCase.contains("coverage"))) //Select all changes
//      .union(Metric.getMetric(cc).filter(_.metric.toLowerCase.contains("mutator")))
//      .union(Metric.getMetric(cc).filter(_.metric == "AllClasses"))
//      .distinct

//    toCsv(m.filter( b => b.buildid == "77c6a46e-4e35-4e89-8e0e-11276435bee7" ||
//      b.buildid == "26f8b1d0-3a1f-4be0-9d4b-63b479499d01" ||
//      b.buildid == "dbf84e3b-d1eb-4ee0-9a48-fe92db46af64" ||
//      b.buildid == "ebdd8439-2191-47f9-bc7d-9dd22363ecc7"))

    toCsv(m)
  }

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")
    import cc.implicits._

    execute(cc)
  }
}