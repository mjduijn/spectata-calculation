package nl.tudelft.spectata.calculation.builddatatodata

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{Build, Data}
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import com.datastax.spark.connector._

object ChangeMain {

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")
    import cc.implicits._

//    val buildIds = Build.getBuilds(cc).collect()
//      .filter(_.status == "PREP_DONE")
//      .filter(_.startts != null)
//      .sortWith((l, r) => l.startts.compareTo(r.startts) < 0)
//      .map(_.buildid)
//      .toList

    val buildIds = List("e5e462dd-f474-4d88-9eea-917ad1b8baaa")
//    val buildIds = scala.tools.nsc.io.File("train_build_ids.txt").lines().map(_.split(",").head).toList

    for (b <- buildIds) {
      println(s"Executing changes for ${b}")
      DataChange.run(cc, b)
      assert(Data.getData(cc).filter(_.buildid == b).count() == Data.getBuildData(cc).filter(_.buildid == b).count())
    }

    Build.storeBuild(
      Build.getBuilds(cc)
        .filter(id => buildIds.contains(id))
        .map(b => Build(b.buildid, b.startts, b.projectkey, b.gitbranch, b.jenkinsbuildtag, b.scmrevisions,
          b.parentbuildid, "CHANGE_DONE")).toDS()
    )
  }
}