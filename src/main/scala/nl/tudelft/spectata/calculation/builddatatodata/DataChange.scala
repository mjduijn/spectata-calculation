package nl.tudelft.spectata.calculation.builddatatodata

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel._
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, SQLContext, TypedColumn}
import com.datastax.spark.connector._
import nl.tudelft.spectata.calculation.datatometric.{Concat, Merge}
import org.apache.spark.sql.expressions.Aggregator

import scala.collection.mutable

object DataChange {

  def getOldData(cc: SQLContext, buildId: String): Dataset[Data] = {
    import cc.implicits._
    Build.getBuilds(cc, buildId)
      .filter(b => b.buildid != b.parentbuildid)
      .as("builds")
      .joinWith(Build.getBuilds(cc, buildId).as("parents"), $"builds.parentbuildid" === $"parents.buildid") //Get full parent builds
      .map(_._2).as("pb")
      .joinWith(Data.getData(cc).as("d"), $"pb.buildid" === $"d.buildid")
      .map(_._2)
  }

  def updateOldData(cc: SQLContext, oldData: Dataset[Data], changes: Dataset[Change]): Dataset[Data] = {
    import cc.implicits._

    def toList[I](f: I => String): TypedColumn[I, Seq[String]] =
      new Concat(f).toColumn(cc.implicits.newStringEncoder, cc.implicits.newStringSeqEncoder)

    //Make sure all old issues have some kind of change (even if nothing changed)
//    val allChanges = oldData.flatMap(data => data.refs.map(ref => Change(data.buildid, ref.path, "MODIFIED", List(), List())))
//      .union(changes)
//      .groupBy(change => change.path)
//      .agg(toList(_.linenumbersadded.mkString("+")), toList(_.linenumbersremoved.mkString("+")))
//      .map{case (path, linenumbersadded, linenumbersremoved) =>
//        Change("", path, "MODIFIED", linenumbersremoved.map(_.toInt), linenumbersadded.map(_.toInt))}

    def merge(f: Data => Seq[CodeRef]): TypedColumn[Data, Seq[CodeRef]] = {
      import cc.implicits._
      new Merge(f).toColumn
    }

    oldData.flatMap(_.toSingleData())
      .as("d").joinWith(changes.as("c"), $"d.ref.path" === $"c.path", "left_outer")
//      .as("d").joinWith(allChanges.as("c"), $"d.ref.path" === $"c.path")
      .map{case (data, change) => {
        var ctr = 0
        val oldLineNumbers = data.ref.linenumbers.sorted.toBuffer
        val newLineNumbers = scala.collection.mutable.ListBuffer.empty[Int]
        val removed = if(change.linenumbersremoved != null) change.linenumbersremoved.sorted.toBuffer else mutable.Buffer[Int]()
        val added = if(change.linenumbersadded != null) change.linenumbersadded.sorted.toBuffer else mutable.Buffer[Int]()
        val largeValue = 99999

        while(oldLineNumbers.nonEmpty) {
          if(removed.fold(largeValue)(_ min _) == added.fold(largeValue)(_ min _) - ctr && removed.fold(largeValue)(_ min _) == oldLineNumbers.fold(largeValue)(_ min _)) { //All 3 equal
            newLineNumbers += (oldLineNumbers.head + ctr)
            oldLineNumbers.remove(0)
          }
          else if(removed.nonEmpty && removed.fold(largeValue)(_ min _) == added.fold(largeValue)(_ min _) - ctr ){ //added and removed equal
            added.remove(0)
            removed.remove(0)
          }
          else if(removed.fold(largeValue)(_ min _) == oldLineNumbers.fold(largeValue)(_ min _)) { //Removed and old equal
            newLineNumbers += (oldLineNumbers.head + ctr)
            oldLineNumbers.remove(0)
          }
          else if(added.fold(largeValue)(_ min _) - ctr  == oldLineNumbers.fold(largeValue)(_ min _)) {
            ctr += 1
            added.remove(0)
          }

          else if (removed.fold(largeValue)(_ min _) < Math.min(oldLineNumbers.fold(largeValue)(_ min _), added.fold(largeValue)(_ min _) - ctr )) {
            removed.remove(0)
            ctr -= 1
          }
          else if (added.fold(largeValue)(_ min _) - ctr  < Math.min(removed.fold(largeValue)(_ min _), oldLineNumbers.fold(largeValue)(_ min _))) {
            added.remove(0)
            ctr += 1
          }
          else if (oldLineNumbers.fold(largeValue)(_ min _) < Math.min(removed.fold(largeValue)(_ min _), added.fold(largeValue)(_ min _) - ctr )) {
            newLineNumbers += (oldLineNumbers.head + ctr)
            oldLineNumbers.remove(0)
          }
          else {
            println(oldLineNumbers)
            println(newLineNumbers)
            println(removed)
            println(added)
            assert(false)
          }
        }
        Data(data.buildid, data.dataid, data.rule, data.value, Seq(CodeRef(data.ref.path, data.ref.key, newLineNumbers)))
      }}
      .groupBy(data => (data.buildid, data.dataid, data.rule, data.value))
      .agg(merge(_.refs))
      .map{case ((buildId, dataId, rule, value), refs) => Data(buildId, dataId, rule, value, refs)}
      .union(oldData.filter(_.refs.isEmpty))
  }

  private def unchangedIssuePairs(cc: SQLContext, oldIssues: Dataset[Data], newIssues: Dataset[Data]): Dataset[(Data, Data)] = {
    import cc.implicits._
    oldIssues.as("old").joinWith(newIssues.as("new"),
      $"old.rule" === $"new.rule" and
        $"old.value" === $"new.value")
      .filter((datas) => datas._1.refs.sorted == datas._2.refs.sorted)
  }

  def selectNewData(cc: SQLContext, oldData: Dataset[Data], newData: Dataset[Data]) = {
    import cc.implicits._
    newData.subtract(unchangedIssuePairs(cc, oldData, newData).map{case(oldIssue, newIssue) => newIssue})
  }

  def selectOldData(cc: SQLContext, oldData: Dataset[Data], newData: Dataset[Data]) = {
    import cc.implicits._
    unchangedIssuePairs(cc, oldData, newData)
      .map{case(o, n) => Data(n.buildid, o.dataid, o.rule, o.value, o.refs)}
  }

  def run(cc: CassandraSQLContext, buildId: String): Unit = {
    val newData = Data.getBuildData(cc).filter(_.buildid == buildId)
    val changes = Change.getChanges(cc, buildId)
    val updatedOldData = updateOldData(cc, getOldData(cc, buildId), changes)

    selectOldData(cc, updatedOldData, newData).rdd.saveToCassandra(cc.getKeyspace, "data")
    selectNewData(cc, updatedOldData, newData).rdd.saveToCassandra(cc.getKeyspace, "data")
  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

//    val buildId = "eed9cbfd-620a-4a1b-8910-0257841b9662"
//    val newIssues = Issue.getBuildIssues(cc).filter(_.buildid == buildId)
//    val changes = Change.getChanges(cc, buildId)
//    val oldIssues = updateOldIssues(cc, getOldIssues(cc, buildId), changes)
//
//    println("\nNew issues posted to db")
//    selectNewIssues(cc, oldIssues, newIssues)
//      .collect().foreach(println)
//
//    println("\nOld issues posted to db")
//    selectOldIssues(cc, oldIssues, newIssues)
//      .collect().foreach(println)
//
//    println("\nnewIssues")
//    newIssues.collect().foreach(println)
//
//    println("\ngetOldIssues")
//    getOldIssues(cc, buildId)
//      .collect().foreach(println)
//
//    println("\nchanges")
//    changes.collect().foreach(println)

  }
}
