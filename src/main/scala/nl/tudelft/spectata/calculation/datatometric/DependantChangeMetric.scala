package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.{Data, DependantChange, Metric}
import org.apache.spark.sql.{Dataset, SQLContext}
import org.apache.spark.sql.functions._

case class DependantChangeMetric(cc: SQLContext, buildIds: Seq[String]) {
  import cc.implicits._
  val metric = "DependantChange"
  var localData: Dataset[DependantChange] = null
  var localNMetrics: Dataset[Metric] = null

  def getData(): Dataset[DependantChange] = {
    if(localData == null) {
      localData = DependantChange.getDependantChange(cc).filter(b => buildIds.contains(b.buildid))
    }
    localData
  }

  def toMetric(data: Dataset[DependantChange] = getData()): Dataset[Metric] = {
    if(localNMetrics == null) {
      localNMetrics = //data.filter(_.change == "MODIFIED")
      data.groupBy(c => (c.buildid, c.path)).agg(count("*"))
        .map{case ((buildId, path), changeCount) => Metric(buildId, path, metric, changeCount, changeCount, List())}
    }
    localNMetrics
  }

  def run() = {
    Metric.store(toMetric())
  }
}