package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.datatometric.Utils._
import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric, SingleData}
import org.apache.spark.sql.{Dataset, SQLContext}

case class Duration(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  import cc.implicits._
  val rule = "DURATION"
  val metric = "Duration"
}