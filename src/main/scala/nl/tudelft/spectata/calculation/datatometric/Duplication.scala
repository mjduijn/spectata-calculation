package nl.tudelft.spectata.calculation.datatometric

import org.apache.spark.sql.SQLContext

case class Duplication(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  val rule = "Duplicate"
  val metric = "Duplication"
}