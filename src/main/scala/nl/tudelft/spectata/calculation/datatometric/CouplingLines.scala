package nl.tudelft.spectata.calculation.datatometric

import org.apache.spark.sql.SQLContext

case class CouplingLines(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  val rule = "CouplingLines"
  val metric = "CouplingLines"
}