package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.{CodeRef, SingleData}
import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, SQLContext, TypedColumn}
import upickle.default._

class SumOf[I, N : Numeric](f: I => N) extends Aggregator[I, N, N] with Serializable {
  val numeric = implicitly[Numeric[N]]
  override def zero: N = numeric.zero
  override def reduce(b: N, a: I): N = numeric.plus(b, f(a))
  override def merge(b1: N,b2: N): N = numeric.plus(b1, b2)
  override def finish(reduction: N): N = reduction
}


//class ToList[I, A](f: I => A) extends Aggregator[I, Seq[A], Seq[A]] with Serializable {
//  override def zero: Seq[A] = Seq[A]()
//  override def reduce(b: Seq[A], a: I): Seq[A] = Seq(f(a)) ++ b//b :+ f(a)
//  override def merge(b1: Seq[A], b2: Seq[A]): Seq[A] = b1 ++ b2
//  override def finish(b: Seq[A]): Seq[A] = b
//}
//
//class ToList[I](f: I => String) extends Aggregator[I, Seq[String], Seq[String]] with Serializable {
//  override def zero: Seq[String] = Seq[String]()
//  override def reduce(b: Seq[String], a: I): Seq[String] = b ++ f(a).map(_.toString)
//  override def merge(b1: Seq[String], b2: Seq[String]): Seq[String] = b1 ++ b2
//  override def finish(b: Seq[String]): Seq[String] = b
//}

//class ToList[I](f: I => Double) extends Aggregator[I, Array[Double], Array[Double]] with Serializable {
//  override def zero: Array[Double] = Array[Double]()
//  override def reduce(b: Array[Double], a: I): Array[Double] = b :+ f(a)
//  override def merge(b1: Array[Double], b2: Array[Double]): Array[Double] = b1 ++ b2
//  override def finish(b: Array[Double]): Array[Double] = b
//}
//
//sealed trait LinkedList [+E] {
//
//  // map implementation above here
//
//  @tailrec final def foldLeft[B]( accumulator : B )( f : (B,E) => B ) : B = {
//    this match {
//      case Node(head, tail) => {
//        val current = f(accumulator, head)
//        tail.foldLeft(current)(f)
//      }
//      case Empty => accumulator
//    }
//  }
//  def map[R]( f : E => R ) : LinkedList[R]
//}
//
//case class Node[+E]( val head : E, val tail : LinkedList[E]  ) extends LinkedList[E] {
//  override def map[R]( f : E => R ) : LinkedList[R] = Node(f(head), tail.map(f))
//}
//
//case object Empty extends LinkedList[Nothing] {
//  override def map[R]( f : Nothing => R ) : LinkedList[R] = this
//}

//case class LinkedList[A](a: Seq[A])
//class Merge[I, A](f: I => LinkedList[A]) extends Aggregator[I, LinkedList[A], Seq[A]] with Serializable {
//  override def zero: LinkedList[A] = Empty
//  override def reduce(b: LinkedList[A], input: I): LinkedList[A] = {
//    val r = f(input).foldLeft(b)((b, a) => Node(a, b))
//    println(r)
//    r
//  }
//  override def merge(b1: LinkedList[A], b2: LinkedList[A]): LinkedList[A] = {
////    println("b1: " + b1.toString() + "    b2: " + b2.toString() + "    b1 ++ b2: " + (b1 ++ b2).toSeq.toString())
//    //    (b1 ++ b2).toList
//    println(b1)
//    println(b2)
//    b1
////    LinkedList(b1.a ++ b2.a)
//  }
//  override def finish(b: LinkedList[A]): Seq[A] = b.foldLeft(Seq[A]())((b, a) => b :+ a)
//}
class Merge[I](f: I => Seq[CodeRef]) extends Aggregator[I, String, Seq[CodeRef]] with Serializable {
  override def zero: String = write(Seq[CodeRef]())
  override def reduce(b: String, a: I): String = write(read[Seq[CodeRef]](b) ++ f(a))
  override def merge(b1: String, b2: String): String = write(read[Seq[CodeRef]](b1) ++ read[Seq[CodeRef]](b2))
  override def finish(b: String): Seq[CodeRef] = read[Seq[CodeRef]](b)
}

class Collect[I](f: I => String) extends Aggregator[I, String, Seq[String]] with Serializable {
  override def zero: String = write(Seq[String]())
  override def reduce(b: String, a: I): String = write(read[Seq[String]](b) :+ f(a))
  override def merge(b1: String, b2: String): String = write(read[Seq[String]](b1) ++ read[Seq[String]](b2))
  override def finish(b: String): Seq[String] = read[Seq[String]](b)
}

class CollectSingleData[I](f: I => SingleData) extends Aggregator[I, String, Seq[SingleData]] with Serializable {
  override def zero: String = write(Seq[SingleData]())
  override def reduce(b: String, a: I): String = write(read[Seq[SingleData]](b) :+ f(a))
  override def merge(b1: String, b2: String): String = write(read[Seq[SingleData]](b1) ++ read[Seq[SingleData]](b2))
  override def finish(b: String): Seq[SingleData] = read[Seq[SingleData]](b)
}
//
//class Merge[I](f: I => Array[Int]) extends Aggregator[I, Array[Int], Array[Int]] with Serializable {
//  override def zero: Array[Int] = mutable.WrappedArray.empty.toArray
//  override def reduce(b: Array[Int], a: I): Array[Int] = {
////    f(a) match {
////      case b: mutable.WrappedArray => println("Wrapped array!")
////    }
//  //    (b ++ f(a))
//    //    val bc = Array[Int]()
//    //    f(a).copyToArray(bc)
//    //    f(a).copyToArray(b)
//    //    println(b.toList)
//    //    (b ++ f(a))
////    val bc = Array[Int](99)
//    //    b.copyToArray(bc)
//    //    f(a).copyToArray(bc)
//    //    bc
//    val buff = scala.collection.mutable.ArrayBuffer.empty[Int]
//
//    f(a).foreach(buff += _)
//    b.foreach(buff += _)
//    println("b: " + b.toList + "    f(a): " + f(a).toList + "    b ++ f(a): " + buff.toArray.toList)
//    return buff.toArray
//  }
//  override def merge(b1: Array[Int], b2: Array[Int]): Array[Int] = {
//    b1.foreach(println)
//    b2.foreach(println)
//    println("b1: " + b1.toList + "    b2: " + b2.toList + "    b1 ++ b2: " + (b1 ++ b2).toList)
//    (b1 ++ b2)
//  }
//  override def finish(b: Array[Int]): Array[Int] = b
//}

class Concat[I](f: I => String) extends Aggregator[I, String, Seq[String]] {
  override def zero: String = ""
  override def reduce(b: String, a: I): String = b + "+" + f(a)
  override def merge(b1: String, b2: String): String = b1 + "+" + b2
  override def finish(b: String): Seq[String] = b.split("\\+").filter(!_.isEmpty).toSeq
}

object Utils {
  def sum[I, N : Numeric : Encoder](f: I => N): TypedColumn[I, N] = new SumOf(f).toColumn
  def sum[N : Numeric : Encoder](): TypedColumn[N, N] = new SumOf(identity[N]).toColumn

  def collectList[I](cc: SQLContext, f: I => String): TypedColumn[I, Seq[String]] = {
    import cc.implicits._
    new Collect(f).toColumn
  }

  def collectSingleDataList[I](cc: SQLContext, f: I => SingleData): TypedColumn[I, Seq[SingleData]] = {
    import cc.implicits._
    new CollectSingleData(f).toColumn
  }
}
