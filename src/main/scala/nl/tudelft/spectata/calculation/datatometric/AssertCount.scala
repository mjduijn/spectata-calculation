package nl.tudelft.spectata.calculation.datatometric

import org.apache.spark.sql.SQLContext

case class AssertCount(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  val rule = "AssertCount"
  val metric = "AssertCount"
}