package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.sql.{Dataset, SQLContext}
import org.apache.spark.sql.functions._

case class AllClasses(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  import cc.implicits._
  val rule = "LINES"
  val metric = "AllClasses"

  override def toMetric(data: Dataset[Data]): Dataset[Metric] = {
    if(localNNMetrics == null) {
      localNNMetrics = data.flatMap(_.toSingleData())
        .filter(_.ref.path.toLowerCase.endsWith("java"))
        .groupBy(d => (d.buildid, d.ref.path))
        .agg(count("*"))
        .map{case ((buildId, path), value) => {
          assert(value == 1)
          Metric(buildId, path, metric, 1, 1, Seq())
        }}
      localNMetrics = localNNMetrics
    }
    localNNMetrics
  }
}