package nl.tudelft.spectata.calculation.datatometric

import org.apache.spark.sql.SQLContext

case class BranchCoverage(cc: SQLContext, buildIds: Seq[String]) extends Coverage {
  val rule = "branch_coverage"
  val metric = "BranchCoverage"
}