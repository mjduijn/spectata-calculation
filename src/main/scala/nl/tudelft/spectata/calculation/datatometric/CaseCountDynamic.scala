package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.datatometric.Utils._
import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SQLContext}

case class CaseCountDynamic(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  val rule = "DURATION"
  val metric = "DynamicCaseCount"

  override def toMetric(data: Dataset[Data] = getData()): Dataset[Metric] = {
    import cc.implicits._
    data.flatMap(_.toSingleData())
      .groupBy(data => (data.buildid, data.ref.path, data.rule))
      .agg(collectList(cc, _.dataid), count("*"))
      .map { case ((buildId, path, _), references, value) => Metric(buildId, path, metric, value, -1, references) }
  }

  override def normalizeMetric(metrics: Dataset[Metric] = toMetric()): Dataset[Metric] = {
    import cc.implicits._
    metrics.map(m => Metric(m.buildid, m.path, m.metric, m.value, m.value, m.references))
  }
}