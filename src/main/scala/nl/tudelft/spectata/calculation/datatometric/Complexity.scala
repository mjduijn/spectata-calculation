package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric, SingleData}
import org.apache.spark.sql.{Dataset, SQLContext}
import nl.tudelft.spectata.calculation.datatometric.Utils._

case class Complexity(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  import cc.implicits._
  val rule = "CyclomaticComplexity"
  val metric = "CyclomaticComplexity"

  override def toMetric(data: Dataset[Data]): Dataset[Metric] = {
    if (localNNMetrics == null) {
      localNNMetrics = data.flatMap(_.toSingleData())
        .groupBy(data => (data.buildid, data.ref.path, data.rule))
        .agg(collectList(cc, _.dataid), sum(_.value.toDouble))
        .map{case ((buildId, path, _), references, value) => Metric(buildId, path, metric, value, -1, references)}
//      localNNMetrics = data.flatMap(_.toSingleData())
//        .map(d => SingleData(d.buildid, d.dataid, d.rule,
//          (d.value.toDouble - 1).toString // Only count additional complexity (i.e. > 1)
//          , d.ref))
//        .groupBy(data => (data.buildid, data.ref.path, data.rule))
//        .agg(collectList(cc, _.dataid), sum(_.value.toDouble))
//        .map{case ((buildId, path, _), references, value) => Metric(buildId, path, metric, value, -1, references)}
    }
    localNNMetrics
  }
}