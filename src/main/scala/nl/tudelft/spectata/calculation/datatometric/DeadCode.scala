package nl.tudelft.spectata.calculation.datatometric

import org.apache.spark.sql.SQLContext

case class DeadCode(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  val rule = "DEAD_CODE"
  val metric = "DeadCode"
}