package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.sql.{Dataset, SQLContext}
import org.apache.spark.sql.functions._
import nl.tudelft.spectata.calculation.datatometric.Utils._

case class Mutation(cc: SQLContext, buildIds: Seq[String]) {
  import cc.implicits._
  var localData: Dataset[Data] = null
  var localMetrics: Dataset[Metric] = null


  def getData(): Dataset[Data] = {
    if (localData == null) {
      localData = Data.getData(cc).filter(b => buildIds.contains(b.buildid)).filter(_.rule.contains("mutator"))
    }
    localData
  }
  def toMetric(data: Dataset[Data] = getData()): Dataset[Metric] = {
    if (localMetrics == null) {
//      val specificMutators = data
//        .flatMap(_.toSingleData())
//        .filter(!_.ref.path.startsWith("src/test"))
//        .groupBy(d => (d.buildid, d.rule, d.ref.path))
//        .agg(collectSingleDataList(cc, identity))
//        .map{case ((buildId, rule, path), ds) => {
//          val both = ds.length
//          val killed = ds.count(d => d.value == "KILLED")
//          Metric(buildId, path, "allMutators", both, killed / both.toDouble, ds.map(_.dataid))
//          val both = ds.count(d => d.value == "SURVIVED" || d.value == "KILLED")
//          val killed = ds.count(d => d.value == "KILLED")
////          println(ds.map(_.value))
//          if(both == 0) {
////            println("DIVISION by zero")
//            Metric(buildId, path, rule, both, Double.NaN, ds.map(_.dataid))
//          }
//          else {
//            Metric(buildId, path, rule, both, killed / both.toDouble, ds.map(_.dataid))
//          }
//        }}

      val allMutators = data
        .flatMap(_.toSingleData())
        .filter(!_.ref.path.startsWith("src/test"))
        .groupBy(d => (d.buildid, d.ref.path))
        .agg(collectSingleDataList(cc, identity))
        .map{case ((buildId, path), ds) => {
//          val both = ds.length
//          val killed = ds.count(d => d.value == "KILLED")
//          Metric(buildId, path, "allMutators", both, killed / both.toDouble, ds.map(_.dataid))

          val both = ds.count(d => d.value == "SURVIVED" || d.value == "KILLED")
          val killed = ds.count(d => d.value == "KILLED")
          if(both == 0) {
            Metric(buildId, path, "allMutators", both, Double.NaN, ds.map(_.dataid))
          }
          else {
            Metric(buildId, path, "allMutators", both, killed.toDouble / both.toDouble, ds.map(_.dataid))
          }
        }}
//
//      //Make sure all production classes are represented, even ones without mutators
//      localMetrics = Data.getData(cc)
//        .filter(_.rule == "LINES")
//        .filter(b => buildIds.contains(b.buildid))
//        .flatMap(_.toSingleData())
//        .filter(!_.ref.path.startsWith("src/test")).as("ntc")
//        .joinWith(allMutators.map(m => (m.buildid, m.path)).as("am") ,
//          $"ntc.buildid" === $"am._1" and $"ntc.ref.path" === $"am._2", "left_outer")
////        .map{case (lines, (buildId, path)) => Metric(lines.buildid, lines.ref.path, "allMutators", -1, -1, Seq[String]())}
////
//        .map{case (lines, (buildId, path)) => if(buildId != null) mMetric else
//          Metric(lines.buildid, lines.ref.path, "allMutators", -1, -1, Seq())}
      localMetrics = allMutators
    }
    localMetrics
  }

/*
  def toMetric(data: Dataset[Data] = getData(),
                        testClasses: Dataset[Metric] = CaseCountDynamic(cc, buildIds).toMetric()): Dataset[Metric] = {

    if (localMetrics == null) {
      val mutations = data
        .flatMap(d => {
          if (d.refs.length == 2 &&
            d.refs.head.path.split("/").last.replaceAll("(?i)test", "") == d.refs.last.path.split("/").last.replaceAll("(?i)test", "")) {
            List(d)
          }
          else {
            List[Data]()
          }
        })
        .flatMap(_.toSingleData())
        .filter(!_.ref.path.startsWith("src/test"))

      val coupledMutations = mutations.map(m => (m.ref.path.split("/").last, m)).as("cs")
        .joinWith(testClasses.map(m => (m.buildid, m.path.split("/").last.replaceAll("(?i)test", ""), m.path)).as("ts"),
          $"ts._1" === $"cs._2.buildid" and $"ts._2" === $"cs._1", "left_outer")
        .map{case ((_, d), (_, _, testPath)) =>
          if(testPath == null) ("VirtualTest//" + d.ref.path, d) else (testPath, d)}
        .groupBy(g => (g._2.buildid, g._1, g._2.rule))
        .agg(collectSingleDataList(cc, _._2))




//      val joinedMutations = testClasses
//        .map(m => (m.buildid, m.path.split("/").last.replaceAll("(?i)test", ""), m.path)).as("ts")
//        .joinWith(mutations.map(m => (m.ref.path.split("/").last, m)).as("cs"),
//          $"ts._1" === $"cs._2.buildid" and $"ts._2" === $"cs._1")
//
//      val unJoinedMutations = mutations.subtract(joinedMutations.map{case (_, (_, m)) => m})
//
//      val coupledMutations = unJoinedMutations.map(c => ("VirtualTest//" + c.ref.path, c))
//        .union(joinedMutations.map{case ((_, _, testKey), (_, d)) => (testKey, d)})
//        .groupBy(g => (g._2.buildid, g._1, g._2.rule))
//        .agg(collectSingleDataList(cc, _._2))
//
      val killed = coupledMutations
        .map{case ((buildId, testPath, r), ds) =>
          Metric(buildId, testPath, r, ds.count(_.value == "KILLED"), -1, ds.map(_.dataid))}

      val both = coupledMutations
        .map{case ((buildId, testPath, r), ds) =>
          Metric(buildId, testPath, r, ds.count(d => d.value == "SURVIVED" || d.value == "KILLED"), -1, ds.map(_.dataid))}

      localMetrics = killed.as("k").joinWith(both.as("b"), $"k.buildid" === $"b.buildid" and $"k.path" === $"b.path" and $"k.metric" === $"b.metric")
        .map{case (metric, normalizer) =>
          Metric(metric.buildid, metric.path, metric.metric, metric.value, metric.value / normalizer.value, metric.references)}

    }
    localMetrics
  }
  */

  def run(): Unit = Metric.store(toMetric(getData()))
}