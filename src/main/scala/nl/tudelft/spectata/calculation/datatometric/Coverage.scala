package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.datatometric.Utils._
import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.sql.{Dataset, SQLContext}

trait Coverage extends MetricCalc {
  import cc.implicits._

//  override def toMetric(data: Dataset[Data] = getData()): Dataset[Metric] = {
//    if (localNNMetrics == null) {
//      val coverages = data.flatMap(_.toSingleData())
//
//      val joinedCoverages = CaseCountDynamic(cc, buildIds).toMetric()
//        .map(m => (m.buildid, m.path.split("/").last.replaceAll("(?i)test", ""), m.path)).as("ts")
//        .joinWith(coverages.map(c => (c.ref.path.split("/").last, c)).as("cs"),
//          $"ts._1" === $"cs._2.buildid" and $"ts._2" === $"cs._1")
//
//      val unJoinedCoverages = coverages.subtract(joinedCoverages.map{case (_, (_, d)) => d})
//
//      localNNMetrics = unJoinedCoverages.map(c => ("VirtualTest//" + c.ref.path, c))
//        .union(joinedCoverages.map{case ((_, _, testKey), (_, d)) => (testKey, d)})
//        .groupBy(g => (g._2.buildid, g._1))
//        .agg(collectSingleDataList(cc, _._2))
//        .map{case ((buildId, testPath), ds) =>
//          Metric(buildId, testPath, metric, ds.map(_.value).sum.toDouble / ds.length, ds.map(_.value.toDouble).sum / ds.length, ds.map(_.dataid))}
//    }
//    localNNMetrics
//  }

  override def toMetric(data: Dataset[Data] = getData()): Dataset[Metric] = {
    if (localNNMetrics == null) {
      localNNMetrics = data.flatMap(_.toSingleData())
        .map(d => Metric(d.buildid, d.ref.path, metric, d.value.toDouble, d.value.toDouble, List(d.dataid)))
      assert(localNNMetrics.map(m => (m.buildid, m.path)).distinct.count() == localNNMetrics.count())
    }
    localNNMetrics
  }

  override def normalizeMetric(metrics: Dataset[Metric] = toMetric()): Dataset[Metric] = metrics
//    metrics.map(m => Metric(m.buildid, m.path, m.metric, m.value, m.value, m.references))
}
