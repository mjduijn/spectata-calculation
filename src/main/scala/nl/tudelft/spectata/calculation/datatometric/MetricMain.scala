package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.Build
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext

object MetricMain {

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

//    val buildIds = Build.getBuilds(cc).collect().filter(_.status == "CHANGE_DONE").map(_.buildid).toList
//    val buildIds = List("e5e462dd-f474-4d88-9eea-917ad1b8baaa")
    val buildIds = scala.tools.nsc.io.File("train_build_ids.txt").lines().map(_.split(",").head).toList

    println(s"Executing metric calculation for $buildIds")
//    CaseCountDynamic(cc, buildIds).run()
    Complexity(cc, buildIds).run()
//    Duration(cc, buildIds).run()
//    ClassLength(cc, buildIds).run()
//    CaseLength(cc, buildIds).run()
//    AssertCount(cc, buildIds).run()
//    DeadCode(cc, buildIds).run()
//    Duplication(cc, buildIds).run()
//    CouplingClasses(cc, buildIds).run()
//    CouplingLines(cc, buildIds).run()
//
//    Mutation(cc, buildIds).run()
//
//    LineCoverage(cc, buildIds).run()
//    BranchCoverage(cc, buildIds).run()
//
//    DependantChangeMetric(cc, buildIds).run()
//    AllClasses(cc, buildIds).run()
  }
}
