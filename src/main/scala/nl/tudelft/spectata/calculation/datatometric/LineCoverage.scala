package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.Metric
import org.apache.spark.sql.{Dataset, SQLContext}

case class LineCoverage(cc: SQLContext, buildIds: Seq[String]) extends Coverage {
  val rule = "line_coverage"
  val metric = "LineCoverage"
}