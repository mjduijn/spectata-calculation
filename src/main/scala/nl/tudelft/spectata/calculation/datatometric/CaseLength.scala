package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.sql.{Dataset, SQLContext}
import nl.tudelft.spectata.calculation.datatometric.Utils._

case class CaseLength(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  import cc.implicits._
  val rule = "MethodLineCount"
  val metric = "CaseLength"

  override def toMetric(data: Dataset[Data] = getData()): Dataset[Metric] = {
    if (localNNMetrics == null) {
      localNNMetrics = data.flatMap(_.toSingleData()).as("m")
        .groupBy(data => (data.buildid, data.ref.path, data.rule))
        .agg(collectList(cc, _.dataid), sum(_.value.toDouble))
        .map{case ((buildId, path, _), references, value) => Metric(buildId, path, metric, value, -1, references)}
    }
    localNNMetrics
  }
}