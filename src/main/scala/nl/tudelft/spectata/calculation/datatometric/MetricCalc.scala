package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.datatometric.Utils.{collectList, sum}
import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.sql.{Dataset, SQLContext}

trait MetricCalc {
  def rule: String
  def metric: String
  def buildIds: Seq[String]
  val cc: SQLContext
  import cc.implicits._
  var localData: Dataset[Data] = null
  var localNNMetrics: Dataset[Metric] = null
  var localNMetrics: Dataset[Metric] = null

  def getData(): Dataset[Data] = {
    if(localData == null) {
      localData = Data.getData(cc).filter(b => buildIds.contains(b.buildid)).filter(_.rule == rule)
    }
    localData
  }

  def toMetric(data: Dataset[Data] = getData()): Dataset[Metric] = {
    if (localNNMetrics == null) {
      localNNMetrics = data.flatMap(_.toSingleData())
        .groupBy(data => (data.buildid, data.ref.path, data.rule))
        .agg(collectList(cc, _.dataid), sum(_.value.toDouble))
        .map{case ((buildId, path, _), references, value) => Metric(buildId, path, metric, value, -1, references)}
    }
    localNNMetrics
  }

  def normalize(metrics: Dataset[Metric], normalizer: MetricCalc): Dataset[Metric] = {
    normalizer.toMetric(normalizer.getData()).as("n")
      .joinWith(metrics.as("m"), $"n.buildid" === $"m.buildid" and $"n.path" === $"m.path")
      .map { case (normalizer, metric) => Metric(metric.buildid, metric.path, metric.metric, metric.value,
        metric.value / normalizer.value, metric.references)
      }
  }

  def normalizeMetric(metrics: Dataset[Metric] = toMetric()): Dataset[Metric] = {
    if (localNMetrics == null) {
      localNMetrics = normalize(metrics, CaseCountDynamic(cc, buildIds))
    }
    localNMetrics
  }

  def run(): Unit = {
    val data = getData()
    val nnmetrics = toMetric(data)
    val nmetrics = normalizeMetric(nnmetrics)
    Metric.store(nmetrics)
  }
}
