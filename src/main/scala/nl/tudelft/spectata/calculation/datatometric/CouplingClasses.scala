package nl.tudelft.spectata.calculation.datatometric

import org.apache.spark.sql.SQLContext

case class CouplingClasses(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  val rule = "CouplingClasses"
  val metric = "CouplingClasses"
}