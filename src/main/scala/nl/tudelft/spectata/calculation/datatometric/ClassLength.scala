package nl.tudelft.spectata.calculation.datatometric

import nl.tudelft.spectata.calculation.datatometric.Utils._
import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SQLContext}

case class ClassLength(cc: SQLContext, buildIds: Seq[String]) extends MetricCalc {
  import cc.implicits._
  val rule = "COVERAGE_LINE_HITS_COUNT"
  val metric = "ClassLength"

  override def normalizeMetric(metrics: Dataset[Metric] = toMetric()): Dataset[Metric] = {
    metrics.map(m => Metric(m.buildid, m.path, m.metric, m.value, m.value, m.references))
      .as("m") //Make sure that only test classes are selected
      .joinWith(CaseCountDynamic(cc, buildIds).toMetric().as("cc"), $"m.buildid" === $"cc.buildid" and $"m.path" === $"cc.path")
      .map(_._1)
  }
  //TODO surround with if
}