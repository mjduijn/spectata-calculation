package nl.tudelft.spectata.calculation.builddataprep

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{Build, TestCoupling}
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext

object PrepMain {

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")
    import cc.implicits._

//    val buildIds = Build.getBuilds(cc).collect()
//      .filter(_.status == "EXECUTION_DONE")
//      .filter(_.startts != null)
//      .sortWith((l, r) => l.startts.compareTo(r.startts) < 0)
//      .map(_.buildid)
//      .toList
//    val buildIds = List("bb2b969a-263e-4b46-9eb8-aafdea0a267f")
//    val buildIds = List("bb2b969a-263e-4b46-9eb8-aafdea0a267f")
    val buildIds = scala.tools.nsc.io.File("train_build_ids.txt").lines().map(_.split(",").head).toList

    println(s"Executing build data prep calculation for $buildIds")

    CouplingLines(cc, buildIds).run()
    CouplingClasses(cc, buildIds).run()

    Build.storeBuild(
      Build.getBuilds(cc)
      .filter(id => buildIds.contains(id))
      .map(b => Build(b.buildid, b.startts, b.projectkey, b.gitbranch, b.jenkinsbuildtag, b.scmrevisions,
      b.parentbuildid, "PREP_DONE")).toDS()
    )
  }
}