package nl.tudelft.spectata.calculation.builddataprep

import java.util.UUID

import nl.tudelft.spectata.calculation.storagemodel.{CodeRef, Data, TestCoupling}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SQLContext}

case class CouplingClasses(cc: SQLContext, buildIds: Seq[String]) {
  val rule = "CouplingClasses"
  import cc.implicits._
  //Calculate the number of files executed per test case

  def calc(): Dataset[Data] = {
    TestCoupling.get(cc).filter(c => buildIds.contains(c.buildid))
      .groupBy(c => (c.buildid, c.testkey))
      .agg(countDistinct("productionkey").as[Long])
      .map{case ((buildId, testKey), value) => Data(buildId, UUID.randomUUID().toString, rule, value.toString, List(
        CodeRef(testKey.replaceAll("\\.java.*", ".java"), testKey, List())))}
  }

  def run(): Unit = Data.storeBuildData(calc())

}