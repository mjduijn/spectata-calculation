package nl.tudelft.spectata.calculation.builddataprep

import java.util.UUID

import com.datastax.spark.connector._
import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.datatometric.{Concat, Merge}
import nl.tudelft.spectata.calculation.storagemodel._
import org.apache.spark.SparkContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.sql.{Dataset, SQLContext, TypedColumn}
import org.apache.spark.sql.functions._

case class CouplingLines(cc: SQLContext, buildIds: Seq[String]) {
  val rule = "CouplingLines"
  import cc.implicits._
  //Calculate the number of lines executed per test case

  def calc(): Dataset[Data] = {
    TestCoupling.get(cc).filter(c => buildIds.contains(c.buildid))
      .groupBy(c => (c.buildid, c.testkey))
      .agg(count("*"))
      .map{case ((buildId, testKey), value) => Data(buildId, UUID.randomUUID().toString, rule, value.toString, List(
        CodeRef(testKey.replaceAll("\\.java.*", ".java"), testKey, List())))}
  }

  def run(): Unit = Data.storeBuildData(calc())

}