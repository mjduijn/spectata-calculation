package nl.tudelft.spectata.calculation

import nl.tudelft.spectata.calculation.storagemodel.{Build, CodeRef}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.sql.{DataFrame, Encoders, SQLContext}
import org.apache.spark.sql.functions._

/*
    Coverage lines that changed since previous build.
    Issues added/deleted since previous build.
 */

object Main {
  val keyspace = "mykeyspace"

  val conf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("test")
    .set("spark.ui.enabled", "false")
    .set("spark.cassandra.connection.host", "localhost")
    .set("spark.cassandra.connection.port", "9042")
    .set("spark.executor.memory", "4g")

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext(conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace(keyspace)
    cc.sparkContext.setLogLevel("WARN")

    import cc.implicits._
    cc.cassandraSql("SELECT * FROM build")
      .as[Build]
      .collect().foreach(println)
  }
}
