package nl.tudelft.spectata.calculation.storagemodel

import org.apache.spark.sql.{Dataset, SQLContext}

case class TestCoupling(buildid: String, testkey: String, productionkey: String, line: Int) {}

object TestCoupling {
  def get(cc: SQLContext): Dataset[TestCoupling] = {
    import cc.implicits._
    cc.sql("SELECT * FROM buildtestcoupling").as[TestCoupling]
  }
}
