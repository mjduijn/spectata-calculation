package nl.tudelft.spectata.calculation.storagemodel

import org.apache.spark.sql.{Dataset, SQLContext}

case class Change(buildid: String, path: String, change: String, linenumbersremoved: Seq[Int], linenumbersadded: Seq[Int]) {}

object Change {
  def getChanges(cc: SQLContext): Dataset[Change] = {
    import cc.implicits._
    cc.sql(s"SELECT * FROM buildchange").as[Change]
  }

  def getChanges(cc: SQLContext, buildId: String): Dataset[Change] = {
    getChanges(cc).filter(_.buildid == buildId)
  }
}