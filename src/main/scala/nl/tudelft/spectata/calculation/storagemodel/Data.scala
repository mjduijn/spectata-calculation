package nl.tudelft.spectata.calculation.storagemodel

import org.apache.spark.sql.{Dataset, SQLContext}
import com.datastax.spark.connector._
import nl.tudelft.spectata.calculation.Main

case class CodeRef (path: String,
                    key: String,
                    linenumbers: Seq[Int]) extends Ordered[CodeRef]
{
  def compare(that: CodeRef): Int = this.toString.compareTo(that.toString)
}

case class Data(buildid: String,
                dataid: String,
                rule: String,
                value: String,
                refs: Seq[CodeRef]){
  def toSingleData(): Seq[SingleData] = this.refs.map(ref => SingleData(this.buildid, this.dataid, this.rule, this.value, ref))
}

case class SingleData(buildid: String,
                dataid: String,
                rule: String,
                value: String,
                ref: CodeRef) { }

object Data {

//  def getDuplicates(cc: SQLContext, buildId: String): Dataset[Duplicate] = getDuplicates(cc).filter(_.buildid == buildId)

  def getBuildData(cc: SQLContext): Dataset[Data] = {
    import cc.implicits._
    cc.sql(s"SELECT * FROM builddata").as[Data]
  }

  def getData(cc: SQLContext): Dataset[Data] = {
    import cc.implicits._
    cc.sql(s"SELECT * FROM data").as[Data]
  }

  def storeBuildData(data: Dataset[Data]) = {
    data.rdd.saveToCassandra(Main.keyspace, "builddata")
  }
}