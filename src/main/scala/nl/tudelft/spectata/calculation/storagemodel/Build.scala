package nl.tudelft.spectata.calculation.storagemodel

import java.sql.Timestamp

import com.datastax.spark.connector._
import nl.tudelft.spectata.calculation.Main
import org.apache.spark.sql.{Dataset, SQLContext}

case class Build(buildid: String,
                 startts: Timestamp,
                 projectkey: String,
                 gitbranch: String,
                 jenkinsbuildtag: String,
                 scmrevisions: Seq[String],
                 parentbuildid: String,
                 status: String) {}

object Build {
  def getBuilds(cc: SQLContext): Dataset[Build] = {
    import cc.implicits._
    cc.sql(s"SELECT * FROM Build".toLowerCase()).as[Build]
  }

  def getBuilds(cc: SQLContext, buildId: String): Dataset[Build] = {
    import cc.implicits._
    cc.sql(s"""SELECT * FROM Build WHERE buildId = "$buildId"""".toLowerCase).as[Build]
  }

  def storeBuild(builds: Dataset[Build]) = {
    builds.rdd.saveToCassandra(Main.keyspace, "build")
  }
}