package nl.tudelft.spectata.calculation.storagemodel

import org.apache.spark.sql.{Dataset, SQLContext}

case class DependantChange (buildid: String,
                            revision: String,
                            path: String,
                            change: String,
                            linenumbersremoved: Seq[Int],
                            linenumbersadded: Seq[Int]) {}

object DependantChange {
  def getDependantChange(cc: SQLContext): Dataset[DependantChange] = {
    import cc.implicits._
    cc.sql(s"SELECT * FROM dependantchange").as[DependantChange]
  }

}