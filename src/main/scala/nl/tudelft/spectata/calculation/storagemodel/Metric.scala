package nl.tudelft.spectata.calculation.storagemodel

import org.apache.spark.sql.{Dataset, SQLContext}
import com.datastax.spark.connector._
import nl.tudelft.spectata.calculation.Main

case class Metric(buildid: String,
                  path: String,
                  metric: String,
                  value: Double,
                  normalizedvalue: Double,
                  references: Seq[String]) {}

object Metric {

  def getMetric(cc: SQLContext): Dataset[Metric] = {
    import cc.implicits._
    cc.sql(s"SELECT * FROM metric").as[Metric]
  }

  def store(metrics: Dataset[Metric]): Unit =
    metrics.rdd.saveToCassandra(Main.keyspace, "metric")
}