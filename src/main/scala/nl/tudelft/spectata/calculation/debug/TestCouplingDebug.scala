package nl.tudelft.spectata.calculation.debug

import java.util.UUID

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{CodeRef, Data, Metric, TestCoupling}
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.sql.functions._

object TestCouplingDebug {

  def execute(cc: SQLContext) = {
    import cc.implicits._

    TestCoupling.get(cc)
      .filter(_.buildid == "087ffe5a-f537-4a67-aa4d-7bf7be5b6c53")
      .collect().foreach(println)

  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

    execute(cc)
  }
}
