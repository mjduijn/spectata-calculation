package nl.tudelft.spectata.calculation.debug

import java.util.UUID

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{CodeRef, Data}
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.cassandra.CassandraSQLContext

object DataDebug {

  def execute(cc: SQLContext) = {

    val rule = List("JUnitStaticSuite", "JUnitSpelling", "UseAssertEqualsInsteadOfAssertTrue",
      "UseAssertSameInsteadOfAssertTrue", "UseAssertNullInsteadOfAssertTrue", "TestClassWithoutTestCases")

    import cc.implicits._
    Data.getBuildData(cc)
      //        .filter(_.rule == "LINES")//COVERAGE_LINE_HITS_COUNT
//      .filter(_.rule == "branch_coverage")
//      .filter(_.rule.contains("mutator"))
//      .filter(_.rule == "DEAD_CODE")
//        .filter(_.rule == "LINES")
//        .flatMap(_.toSingleData())
//        .filter(_.ref.path.contains("src/test"))
//        .filter(_.buildid == "52f50094-13eb-4f3c-a69a-875e7bc07a39")
//        .filter(_.rule == "DURATION")
//        .filter(_.buildid == "bc3b6da6-0023-46d9-b64c-22b9308aa183")
//        .filter(_.buildid == "dbf84e3b-d1eb-4ee0-9a48-fe92db46af64")

//      .filter(_.buildid == "26f8b1d0-3a1f-4be0-9d4b-63b479499d01")
      .filter(_.buildid == "e5e462dd-f474-4d88-9eea-917ad1b8baaa")
//        .filter(_.buildid == "2e79db62-5754-448b-9ddb-3797ba542337")
//      .collect().foreach(println)
//      .flatMap(_.toSingleData())
//      .filter(_.ref.path.contains("CoursesTest.java"))
//      .filter(_.ref.path.contains("PMDWarningGeneratorTest.java"))//
//        .filter(_.ref.path.contains("FilterOptionIntegrationTest.java"))
//      .filter(d => rule.contains(d.rule))
      .collect().foreach(println)
//
//    println(
//      Data.getBuildData(cc)
//        .filter(_.rule.toLowerCase == "duration")
//        .flatMap(_.refs.map(_.path))
//        .distinct
//        .count())

    println()

//    Data.getData(cc)
//      .filter(_.rule == "Duplicate")
//      .collect().foreach(println)

//    val durations = Data.getBuildData(cc)
//      .filter(_.buildid == "087ffe5a-f537-4a67-aa4d-7bf7be5b6c53")
//      .flatMap(_.toSingleData().map(_.ref.path) )
//      .distinct
//      .filter(_.contains("src/test"))
//      .filter(_.toLowerCase.endsWith(".java"))
//      .map(path => Data("087ffe5a-f537-4a67-aa4d-7bf7be5b6c53", UUID.randomUUID().toString, "DURATION", "0.99", Seq(CodeRef(path, path, Seq[Int]()))))
////      .collect().foreach(println)
//    Data.storeBuildData(durations)

  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

    execute(cc)
  }
}
