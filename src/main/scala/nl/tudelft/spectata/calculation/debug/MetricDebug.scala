package nl.tudelft.spectata.calculation.debug

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{Data, Metric}
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.cassandra.CassandraSQLContext

object MetricDebug {

  def execute(cc: SQLContext) = {
    Metric.getMetric(cc)
//        .filter(_.metric == "AssertCount")
//        .filter(_.path == "src/test/java/org/junit/tests/assertion/MultipleFailureExceptionTest.java")
//        .filter(_.metric.toLowerCase.contains("coverage"))
//        .filter(_.metric == "DependantChange")
//        .filter(_.metric == "AllClasses")
//        .fi
//        .filter(_.metric == "DynamicCaseCount")
//        .filter(_.buildid == "52f50094-13eb-4f3c-a69a-875e7bc07a39")
        .filter(_.path.contains("TestIslamicChronology"))
      .filter(_.buildid == "e5e462dd-f474-4d88-9eea-917ad1b8baaa")
//      .filter(_.buildid == "bc3b6da6-0023-46d9-b64c-22b9308aa183")
//      .filter(_.metric == "Duration")
      .collect().foreach(println)
  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

    execute(cc)
  }
}
