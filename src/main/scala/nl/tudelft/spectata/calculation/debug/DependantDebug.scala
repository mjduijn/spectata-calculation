package nl.tudelft.spectata.calculation.debug

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{Data, DependantChange}
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.cassandra.CassandraSQLContext

object DependantDebug {

  def execute(cc: SQLContext) = {
    DependantChange.getDependantChange(cc)
        .filter(_.path.contains("FilterOptionIntegrationTest"))
        .collect().foreach(println)





  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

    execute(cc)
  }
}
