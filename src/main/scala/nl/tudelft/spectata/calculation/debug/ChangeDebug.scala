package nl.tudelft.spectata.calculation.debug

import nl.tudelft.spectata.calculation.Main
import nl.tudelft.spectata.calculation.storagemodel.{Change, Data}
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.cassandra.CassandraSQLContext

object ChangeDebug {

  def execute(cc: SQLContext) = {
    import cc.implicits._
    Change.getChanges(cc, "2e27c25a-41d9-4684-8ea5-aae3c9554ec4")
      .collect().foreach(println)
  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(Main.conf)
    val cc = new CassandraSQLContext(sc)
    cc.setKeyspace("mykeyspace")
    cc.sparkContext.setLogLevel("WARN")

    execute(cc)
  }
}
