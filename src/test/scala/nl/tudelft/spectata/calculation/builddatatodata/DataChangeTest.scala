package nl.tudelft.spectata.calculation.builddatatodata

import com.holdenkarau.spark.testing.SharedSparkContext
import nl.tudelft.spectata.calculation.storagemodel.{Change, CodeRef, Data}
import org.apache.spark.sql.SQLContext
import org.scalatest.FunSuite
import org.scalatest.Matchers._

class DataChangeTest extends FunSuite with SharedSparkContext {
  val buildId = "d46b417d-2c80-42e4-b6c0-f90a5a262008"
  val parentBuildId = "d46b417d-2c80-42e4-b6c0-f90a5a262007"
  val path1 = "src/main/main.java"

  test("updateOldData test1") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List()))
    val changes = Seq(Change(buildId, path1, "MODIFIED", linenumbersremoved = List(1, 2), linenumbersadded = List()))

    val expected = Seq(Data(buildId, "id1", "rule1", "", List()))
    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("updateOldData test2") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(3, 4, 5)))))
    val changes = Seq(Change(buildId, path1, "MODIFIED", linenumbersremoved = List(1, 2), linenumbersadded = List()))

    val expected = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(1, 2, 3)))))
    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }


  test("updateOldData test2b") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(3, 4, 5)))))
    val changes = Seq(Change(buildId, path1, "MODIFIED", linenumbersremoved = List(1, 2), linenumbersadded = List(1, 2))).toDS()

    val expected = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(3, 4, 5)))))
    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("updateOldData test3a") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(1, 2)))))
    val changes = Seq(Change(buildId, path1, "MODIFIED", linenumbersremoved = List(1, 2), linenumbersadded = List(1, 2))).toDS()

    val expected = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(1, 2)))))
    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("updateOldData test3b") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))
    val changes = Seq(
      Change(buildId, path1, "MODIFIED", linenumbersremoved = List(1, 2, 8, 9), linenumbersadded = List(6, 7)),
      Change(buildId, path1 + "otherpath", "MODIFIED", linenumbersremoved = List(1, 2), linenumbersadded = List(1, 2))).toDS()

    val expected = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(6, 7)))))
    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("updateOldData test4") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "1.28983", List(CodeRef(path1, path1, List(8, 9)))))
    val changes = Seq[Change]().toDS()

    val expected = Seq(Data(buildId, "id1", "rule1", "1.28983", List(CodeRef(path1, path1, List(8, 9)))))
    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("updateOldData test5") {

    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data("4d9e06d4-99d0-4482-ac69-f6716cd556e3", "1d2a49cd-6902-42f0-9f9c-6a87df763176",
      "CyclomaticComplexity", "", List(CodeRef("src/test/java/com/ing/example/OneTest.java",
        "src/test/java/com/ing/example/OneTest.java/testMethodWithMutation2", List(59, 60, 61, 62, 63)))))

    val changes = Seq(
      Change("dfcc18f3-9ee7-4486-ac67-5ead22eee13f","src/test/java/com/ing/example/OneTest.java","MODIFIED",List(52, 66),
        List(52, 53, 54, 55, 56, 70))
    )

    val expected = Seq(
      Data("4d9e06d4-99d0-4482-ac69-f6716cd556e3","1d2a49cd-6902-42f0-9f9c-6a87df763176",
        "CyclomaticComplexity", "", List(CodeRef("src/test/java/com/ing/example/OneTest.java",
          "src/test/java/com/ing/example/OneTest.java/testMethodWithMutation2", List(63, 64, 65, 66, 67)))))

    val result = DataChange.updateOldData(cc, oldData.toDS(), changes.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("selectNewData test1") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))
    val newData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))

    val expected = Seq()
    val result = DataChange.selectNewData(cc, oldData.toDS(), newData.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("selectNewData test2") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))
    val newData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1,List(12, 13, 14)))))

    val expected = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1,List(12, 13, 14)))))
    val result = DataChange.selectNewData(cc, oldData.toDS(), newData.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("selectOldData test1") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))
    val newData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))

    val expected = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))
    val result = DataChange.selectOldData(cc, oldData.toDS(), newData.toDS()).collect()

    result should contain theSameElementsAs expected
  }

  test("selectOldData test2") {
    val cc = new SQLContext(sc)
    import cc.implicits._

    val oldData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1, List(8, 9)))))
    val newData = Seq(Data(buildId, "id1", "rule1", "", List(CodeRef(path1, path1,List(12, 13, 14)))))

    val expected = Seq()
    val result = DataChange.selectOldData(cc, oldData.toDS(), newData.toDS()).collect()

    result should contain theSameElementsAs expected
  }
}
