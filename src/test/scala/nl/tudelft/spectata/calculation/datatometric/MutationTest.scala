package nl.tudelft.spectata.calculation.datatometric

import com.holdenkarau.spark.testing.SharedSparkContext
import nl.tudelft.spectata.calculation.storagemodel.{Change, CodeRef, Data, Metric}
import org.apache.spark.sql.SQLContext
import org.scalatest.FunSuite
import org.scalatest.Matchers._

class MutationTest extends FunSuite with SharedSparkContext {
  val buildId = "d46b417d-2c80-42e4-b6c0-f90a5a262008"
  val sourcePath1 = "src/main/main.java"
  val testPath1 = "src/test/mainTest.java"
  val testKey1 = "src/test/mainTest.java/test1"
  val mutator = "m1"

//  test("toMetric test1") {
//    val cc = new SQLContext(sc)
//    import cc.implicits._
//
//    val data = Seq(Data(buildId, "dataid1", mutator, "KILLED", List(CodeRef(sourcePath1, sourcePath1, List(1)), CodeRef(testPath1, testPath1, List(1)))))
//    val testClasses = Seq(Metric(buildId, testPath1, "metric", 0, 0, List()))
//
//    val result = Mutation(cc, List(buildId)).toMetric(data.toDS(), testClasses.toDS()).collect()
//    val expected = Seq(Metric(buildId, testPath1, mutator, 1, 1, List("dataid1")))
//
//    result should contain theSameElementsAs expected
//  }
//
//  test("toMetric test2") {
//    val cc = new SQLContext(sc)
//    import cc.implicits._
//
//    val data = Seq(Data(buildId, "dataid1", mutator, "KILLED", List(CodeRef(sourcePath1, sourcePath1, List(1)), CodeRef(testPath1, testPath1, List(1)))),
//      Data(buildId, "dataid1", mutator, "SURVIVED", List(CodeRef(sourcePath1, sourcePath1, List(1)), CodeRef(testPath1, testPath1, List(1)))))
//    val testClasses = Seq(Metric(buildId, testPath1, "metric", 0, 0, List()))
//
//    val result = Mutation(cc, List(buildId)).toMetric(data.toDS(), testClasses.toDS()).collect()
//    val expected = Seq(Metric(buildId, testPath1, mutator, 1.0, 0.5, List("dataid1", "dataid1")))
//
//    result should contain theSameElementsAs expected
//  }
//
//  test("toMetric test3") {
//    val cc = new SQLContext(sc)
//    import cc.implicits._
//
//    val data = Seq(Data(buildId, "dataid1", mutator, "KILLED", List(CodeRef(sourcePath1, sourcePath1, List(1)), CodeRef(testPath1, testPath1, List(1)))))
//    val testClasses = Seq(Metric(buildId, testPath1 + "other", "metric", 0, 0, List()))
//
//    val result = Mutation(cc, List(buildId)).toMetric(data.toDS(), testClasses.toDS()).collect()
//    val expected = Seq(Metric(buildId, "VirtualTest//" + sourcePath1, mutator, 1.0, 1.0, List("dataid1", "dataid1")))
//
//    result should contain theSameElementsAs expected
//  }
}
